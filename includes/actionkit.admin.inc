<?php
/**
 * Code for handling administrative functions for the ActionKit Module.
 *
 * @author Developed by Richir Outreach | http://www.richiroutreach.com
 */

/**
 * Page callback for Actionkit Petitions settings.
 *
 * @see actionkit_petitions_menu()
 */
function actionkit_config_form($form, &$form_state) {
  $form['actionkit_server'] = array(
    '#type'        => 'textfield',
    '#title'      => t('Actionkit Server'),
    '#default_value'  => variable_get('actionkit_server', ""),
    '#size'        => 25,
    '#maxlength'    => 255,
    '#description'    => t('The URL of your ActionKit XML-RPC server. Ex: SLUG.actionkit.com'),
    '#required'      => TRUE,
  );

  $form['actionkit_username'] = array(
    '#type'        => 'textfield',
    '#title'      => t('User Name'),
    '#default_value'  => variable_get('actionkit_username', ""),
    '#size'        => 25,
    '#maxlength'    => 255,
    '#description'    => t('The username for the ActionKit server which will be making the API calls.'),
    '#required'      => TRUE,
  );

  $form['actionkit_password'] = array(
    '#type'        => 'textfield',
    '#title'      => t('Password'),
    '#default_value'  => variable_get('actionkit_password', ""),
    '#size'        => 25,
    '#maxlength'    => 255,
    '#description'    => t('The password for the user the API will be using.'),
    '#required'      => TRUE,
  );

  $form['actionkit_payment_account'] = array(
    '#type'        => 'textfield',
    '#title'      => t('Payment Account'),
    '#default_value'  => variable_get('actionkit_payment_account', ""),
    '#size'        => 25,
    '#maxlength'    => 255,
    '#description'    => t('The exact name of the payment account in ActionKit where donations should be deposited. You might find this at SLUG.actionkit.com/dash/config/edit/Donation Page Defaults/'),
    '#required'      => TRUE,
  );

  $form['actionkit_signature_report'] = array(
    '#type'        => 'textfield',
    '#title'      => t('Signature Report'),
    '#default_value'  => variable_get('actionkit_signature_report', ""),
    '#size'        => 25,
    '#maxlength'    => 255,
    '#description'    => t('The machine name (short name) of your ActionKit report which gives the number of signatures for a given page ID. See INSTALL.txt.'),
    '#required'      => TRUE,
  );

  $form['actionkit_last_signatures_report'] = array(
    '#type'        => 'textfield',
    '#title'      => t('Last Signatures Report'),
    '#default_value'  => variable_get('actionkit_last_signatures_report', ""),
    '#size'        => 25,
    '#maxlength'    => 255,
    '#description'    => t('The machine name of your ActionKit report which returns the last signatures on a Page ID. See INSTALL.txt'),
    '#required'      => TRUE,
  );

  $form['actionkit_action_taken_report'] = array(
    '#type'        => 'textfield',
    '#title'      => t('Action Taken Report'),
    '#default_value'  => variable_get('actionkit_action_taken_report', ""),
    '#size'        => 25,
    '#maxlength'    => 255,
    '#description'    => t('The machine name of your ActionKit report which returns how many times a particular user has taken action on a particular page. See INSTALL.txt'),
    '#required'      => TRUE,
  );

  $form['actionkit_total_donation_amount'] = array(
    '#type'        => 'textfield',
    '#title'      => t('Total Donation Amount'),
    '#default_value'  => variable_get('actionkit_total_donation_amount', ""),
    '#size'        => 25,
    '#maxlength'    => 255,
    '#description'    => t('The machine name of your ActionKit report which returns how much money a page has raised.'),
    '#required'      => TRUE,
  );

  $form['actionkit_petition_signatures'] = array(
    '#type'        => 'textfield',
    '#title'      => t('Petition Signatures'),
    '#default_value'  => variable_get('actionkit_petition_signatures', ""),
    '#size'        => 25,
    '#maxlength'    => 255,
    '#description'    => t('The machine name of your ActionKit report which returns a list of signatures. See INSTALL.txt'),
    '#required'      => TRUE,
  );

  $form['actionkit_page_tag_id'] = array(
    '#type'        => 'textfield',
    '#title'      => t('Page Tag ID'),
    '#description'    => t('The ActionKit ID of the tag you wish to tag all created pages with.'),
    '#required'      => TRUE,
    '#number_type'    => 'integer',
    '#element_validate' => array('element_validate_integer'),
    '#size'        => 5,
    '#default_value'  => variable_get('actionkit_page_tag_id', ""),
  );

  $form['actionkit_petition_slug'] = array(
    '#type'        => 'textfield',
    '#title'      => t('Petition Creator Slug'),
    '#default_value'  => variable_get('actionkit_petition_slug', ""),
    '#size'        => 25,
    '#maxlength'    => 255,
    '#description'    => t('The machine name of your ActionKit petition which is used to tag petition creators.'),
    '#required'      => TRUE,
  );

  $form['actionkit_donate_slug'] = array(
    '#type'        => 'textfield',
    '#title'      => t('Donate Creator Slug'),
    '#default_value'  => variable_get('actionkit_donate_slug', ""),
    '#size'        => 25,
    '#maxlength'    => 255,
    '#description'    => t('The machine name of your ActionKit petition which is used to tag donation page creators.'),
    '#required'      => TRUE,
  );

  $form['actionkit_new_user_text'] = array(
    '#type'        => 'textfield',
    '#title'      => t('New User Introduction Text'),
    '#default_value'  => variable_get('actionkit_new_user_text', ""),
    '#size'        => 25,
    '#maxlength'    => 255,
    '#description'    => t('This text displays on the new user form when someone creates an ActionKit page but is not logged in.'),
    '#required'      => TRUE,
  );

  $form['actionkit_receipt_subject'] = array(
    '#type'        => 'textfield',
    '#title'      => t('Receipt Email Subject'),
    '#default_value'  => variable_get('actionkit_receipt_subject', ""),
    '#size'        => 25,
    '#maxlength'    => 255,
    '#description'    => t('The subject line of the donation receipt email.'),
    '#required'      => TRUE,
  );

  $form['actionkit_receipt_body'] = array(
    '#type'        => 'textfield',
    '#title'      => t('Receipt Email Body'),
    '#default_value'  => variable_get('actionkit_receipt_body', "<p>{{ user.first_name|default:'Friend' }},</p><p>Thank you for your generous donation! The following is a record of your transaction:</p><p>You gave " . '$' . "{{ action.order.total }}." . "Your order ID {{ action.order.id }}.</p><p>Thank you for donating to our cause!</p>"),
    '#size'        => 25,
    '#maxlength'    => 511,
    '#description'    => t('The body text of the donation receipt email.'),
    '#required'      => TRUE,
  );

  return system_settings_form($form);
}
