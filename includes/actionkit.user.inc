<?php
/**
 * Code specific to interaction with ActionKit users.
 *
 * @author Developed by Richir Outreach.
 */

/**
 * Includes.
 */
module_load_include('inc', 'actionkit', 'includes/actionkit.petition');
module_load_include('inc', 'actionkit', 'includes/actionkit.donate');


/**
 * Custom "New User" form.
 */
function actionkit_new_user_form($form, &$form_state) {
  $form['description'] = array(
    '#markup'    => variable_get('actionkit_new_user_text'),
  );

  $form['first_name'] = array(
    '#type'    => 'textfield',
    '#title'    => t('First Name'),
    '#maxlength'  => 50,
    '#description'  => t('Your First Name'),
    '#required'  => TRUE,
  );

  $form['last_name'] = array(
    '#type'    => 'textfield',
    '#title'    => t('Last Name'),
    '#maxlength'  => 50,
    '#description'  => t('Your Last Name'),
    '#required'  => TRUE,
  );

  $form['email'] = array(
    '#type'    => 'textfield',
    '#title'    => t('Email Address'),
    '#maxlength'  => 50,
    '#description'  => t('Your Email Address'),
    '#required'  => TRUE,
  );

  $form['password'] = array(
    '#type'    => 'password',
    '#title'    => t('Password'),
    '#maxlength'  => 25,
    '#description'  => t('Enter a Password'),
    '#required'  => TRUE,
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value'  => t('Join'),
  );

  return $form;
}

/**
 * Validate our new user form.
 */
function actionkit_new_user_form_validate($form, &$form_state) {
  // Ensure this email is not already in use.
  if (db_query("SELECT COUNT(*) FROM {users} WHERE mail = :mail", array(':mail' => $form_state['values']['email']))->fetchField()) {
    form_set_error('email', t("That email address is already in use."));
  }

  // Verify they entered something that at least looks like a real email.
  if (!valid_email_address($form_state['values']['email'])) {
    form_set_error('email', t("Please enter a valid email address"));
  }

  // Make sure the password is at least 6 characters.
  if (strlen($form_state['values']['password']) < 6) {
    form_set_error('password', t("Passwords must be at least 6 characters long"));
  }
}


/**
 * Attempt to create our user.
 */
function actionkit_new_user_form_submit($form, &$form_state) {
  global $base_url;

  // Slug is unused at this time, but we still need it.
  $slug = "";

  // Check if the user exists.
  $user = user_load_by_name($form_state['values']['email']);

  // Does this user exist.
  if (!$user) {

    // Generate the array of information to save.
    $new_user = array(
      'name' => $form_state['values']['email'],
      'mail' => $form_state['values']['email'],
      'user_firstname' => array(
        'und' => array(
          0 => array(
            'value' => $form_state['values']['first_name'],
          ),
        ),
      ),
      'user_lastname' => array(
        'und' => array(
          0 => array(
            'value' => $form_state['values']['last_name'],
          ),
        ),
      ),
      'pass' => $form_state['values']['password'],
      'init' => $form_state['values']['email'],
      'status' => 1,
      'access' => REQUEST_TIME,
    );

    // Save the user.
    $user_data = user_save(NULL, $new_user);

    // Log the user in.
    if ($uid = user_authenticate($new_user['name'], $new_user['pass'])) {
      $user = user_load($uid);
      $login_array = array('name' => $new_user['name']);
      user_login_finalize($login_array);
    }

    // Send a welcome E-Mail.
    _user_mail_notify('register_no_approval_required', $user_data);

    // Determine where the request originates.
    if ($_SESSION['actionkit_source'] == 'petition') {

      // Create our petition now that we have a user.
      $page_id = actionkit_create_petition($_SESSION['actionkit_petition_name'], "", "This petition was created programmatically via Drupal. For information about this petition, please visit " . $base_url, "", "", $slug);

      // Only try to create a node if we made a petition.
      if ($page_id > 0) {

        // Create a node with the actionkit data.
        $redirect = actionkit_petition_node($_SESSION['actionkit_petition_name'], $_SESSION['actionkit_petition_target'], $_SESSION['actionkit_petition_about'], $_SESSION['actionkit_petition_body'],
          $_SESSION['actionkit_petition_thank_you'], $page_id, $slug);

        // Load the user so we can access our custom fields.
        $user_fields = user_load($user->uid);

        // Get our First and Last names.
        $first = field_get_items("user", $user_fields, "user_firstname");
        $last = field_get_items("user", $user_fields, "user_lastname");

        // Sign the dummy petition to tag the user.
        actionkit_dummy_sign("Petition");

        // Create or Update this user in ActionKit.
        actionkit_create_user($user_fields->mail, $first[0]['value'], $last[0]['value']);
      }

      // Redirect us to the petition list.
      $form_state['redirect'] = $redirect;

    }

    elseif ($_SESSION['actionkit_source'] == 'donate') {
      // Try to make our petition and get the page ID.
      $page_id = actionkit_create_donate($_SESSION['actionkit_donate_name'], "This Donation form was created programmatically via Drupal. For information about this petition, please visit " . $base_url, $slug);

      // Only try to create a node if we made a petition.
      if ($page_id > 0) {

        // Create a node with the actionkit data.
        $redirect = actionkit_donate_node($_SESSION['actionkit_donate_name'], $_SESSION['actionkit_donate_body'], $_SESSION['actionkit_donate_goal'], $_SESSION['actionkit_donate_thank_you'], $page_id, $slug);

        // Load the user so we can access our custom fields.
        $user_fields = user_load($user->uid);

        // Get our First and Last names.
        $first = field_get_items("user", $user_fields, "user_firstname");
        $last = field_get_items("user", $user_fields, "user_lastname");

        // Create / Update this user in ActionKit.
        actionkit_create_user($user_fields->mail, $first[0]['value'], $last[0]['value']);

        // Sign the dummy petition to tag the user.
        actionkit_dummy_sign("Donate");

        // Redirect us to the donate page.
        $form_state['redirect'] = $redirect;
      }
    }
  }
}

/**
 * Create or update an AK user.
 */
function actionkit_create_user($email, $first_name, $last_name) {
  // Set up the server connection.
  $server = url('https://' . variable_get('actionkit_username') . ':' . variable_get('actionkit_password') . '@' . variable_get('actionkit_server') . '/api', array('external' => TRUE));

  // Set up the stuff we want to save to AK.
  $options = array(
    'User.save_or_create' => array(
      (object) array(
        "email" => $email,
        "first_name" => $first_name,
        "last_name" => $last_name,
      ),
    ),
  );

  // Run our query.
  $result = xmlrpc($server, $options);

  // Test if our query completed.
  if ($result == FALSE) {
    drupal_set_message(t("ERROR: @var"), array('@var' => xmlrpc_error_msg()), 'status');
  }
}
