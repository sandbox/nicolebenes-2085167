<?php
/**
 * Code specific to ActionKit petition pages.
 *
 * @author Developed by Richir Outreach.
 */

/**
 * Includes.
 */
module_load_include('inc', 'actionkit', 'includes/actionkit.user');
module_load_include('inc', 'actionkit', 'includes/actionkit.general');

/**
 * Form for creating a new petition.
 */
function actionkit_petition_form($form, &$form_state) {

  // Get the default values for this form.
  $default_values = actionkit_default_values('petition');

  $form['name'] = array(
    '#type'      => 'textfield',
    '#title'    => t('Petition Name'),
    '#maxlength'  => 255,
    '#description'  => t('Name your Petition'),
    '#required'    => TRUE,
  );

  $form['target'] = array(
    '#type'      => 'text_format',
    '#format'    => NULL,
    '#base_type'  => 'textarea',
    '#title'    => t('Petition Target'),
    '#description'  => t('Who is your petition for?'),
    '#required'    => TRUE,
    '#default_value' => $default_values['target'],
  );

  $form['about'] = array(
    '#type'      => 'text_format',
    '#format'    => NULL,
    '#base_type'  => 'textarea',
    '#title'    => t('About Petition'),
    '#description'  => t('What is your petition about?'),
    '#required'    => TRUE,
    '#default_value' => $default_values['about'],
  );

  $form['body'] = array(
    '#type'      => 'text_format',
    '#format'    => NULL,
    '#base_type'  => 'textarea',
    '#title'    => t('Petition Text'),
    '#description'  => t('Write your petition'),
    '#required'    => TRUE,
    '#default_value' => $default_values['body'],
  );

  $form['thank_you'] = array(
    '#type'      => 'text_format',
    '#format'    => NULL,
    '#base_type'  => 'textarea',
    '#title'    => t('Thank You Text'),
    '#description'  => t('Text displayed when signed'),
    '#required'    => TRUE,
    '#default_value' => $default_values['thank_you'],
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value'  => t('Create Petition!'),
  );

  return $form;
}


/**
 * Validation for creating a new petition.
 */
function actionkit_petition_form_validate($form, &$form_state) {
  if (actionkit_profanity_check($form_state['values']['name'])) {
    form_set_error('name', t('Your title cannot contain profanity.'));
  }

  if (actionkit_profanity_check($form_state['values']['target']['value'])) {
    form_set_error('target', t('Your target cannot contain profanity.'));
  }

  if (actionkit_profanity_check($form_state['values']['about']['value'])) {
    form_set_error('about', t('Your about text cannot contain profanity.'));
  }

  if (actionkit_profanity_check($form_state['values']['body']['value'])) {
    form_set_error('body', t('Your petition cannot contain profanity.'));
  }

  if (actionkit_profanity_check($form_state['values']['thank_you']['value'])) {
    form_set_error('thank_you', t('Your thank you message cannot contain profanity.'));
  }
}


/**
 * Implements hook_submit().
 */
function actionkit_petition_form_submit($form, &$form_state) {
  global $user;
  global $base_url;

  // Slug of the page we create.
  $slug = "";

  // Was this petition created by someone not logged in?
  if ($user->uid == 0) {

    // Store the form values in some session variables.
    $_SESSION['actionkit_petition_name'] = $form_state['values']['name'];
    $_SESSION['actionkit_petition_target'] = $form_state['values']['target'];
    $_SESSION['actionkit_petition_about'] = $form_state['values']['about'];
    $_SESSION['actionkit_petition_body'] = $form_state['values']['body'];
    $_SESSION['actionkit_petition_thank_you'] = $form_state['values']['thank_you'];
    $_SESSION['actionkit_source'] = 'petition';

    $form_state['redirect'] = "new/account";

    // Go ahead and make the petition since this person is already logged in.
  }

  else {

    // Try to make our petition and get the page ID.
    $page_id = actionkit_create_petition($form_state['values']['name'], "", "This petition was created programmatically via Drupal. For information about this petition, please visit " . $base_url, "", "", $slug);

    // Only try to create a node if we made a petition.
    if ($page_id > 0) {

      // Create a node with the actionkit data.
      $redirect = actionkit_petition_node($form_state['values']['name'], $form_state['values']['target'], $form_state['values']['about'], $form_state['values']['body'], $form_state['values']['thank_you'], $page_id, $slug);

      // Load the user so we can access our custom fields.
      $user_fields = user_load($user->uid);

      // Get our First and Last names.
      $first = field_get_items("user", $user_fields, "user_firstname");
      $last = field_get_items("user", $user_fields, "user_lastname");

      // Create or update this user in ActionKit.
      actionkit_create_user($user_fields->mail, $first[0]['value'], $last[0]['value']);

      // Sign a petition to tag the user as creating a petition.
      actionkit_dummy_sign("Petition");
    }

    $form_state['redirect'] = $redirect;
  }
}


/**
 * Make a node of type actionkit_petition.
 *
 * @title Title of the node to create
 * @page_id Page ID of the actionkit petition
 * @slug Slug of the actionkit petition
 */
function actionkit_petition_node($name, $target, $about, $body, $thank_you, $page_id, $slug) {
  global $user;

  // Create a generic object.
  $node = new stdClass();

  // Node Title.
  $node->title = $name;
  // Content Type of Node.
  $node->type = 'actionkit_petition';
  // 1 = Published.
  $node->status = 1;
  // 0 = Not Promoted to Front Page.
  $node->promote = 0;
  // 0 = No revision.
  $node->revision = 0;
  // Creators's user ID.
  $node->uid = $user->uid;
  // Creator's user name.
  $node->name = $user->name;
  // English.
  $node->language = 'en';
  // 0 = No comments.
  $node->comment = 0;
  // Node creation time.
  $node->created = $_SERVER['REQUEST_TIME'];
  // Node last edited changed time.
  $node->changed = $_SERVER['REQUEST_TIME'];
  // Set the page ID.
  $node->actionkit_page_id[LANGUAGE_NONE][0]['value'] = $page_id;
  // Set the targets.
  $node->actionkit_petition_target[LANGUAGE_NONE][0] = $target;
  // Set the about text.
  $node->actionkit_petition_about[LANGUAGE_NONE][0] = $about;
  // Set the thank you text.
  $node->actionkit_thank_you[LANGUAGE_NONE][0] = $thank_you;
  // Set the petition text.
  $node->body[LANGUAGE_NONE][0] = $body;
  // Set the petition slug.
  $node->actionkit_slug[LANGUAGE_NONE][0]['value'] = $slug;

  // Submit and Save the node.
  node_submit($node);
  node_save($node);

  // Find our the path of the node we created, it's alias if possible.
  $path = drupal_get_path_alias("node/" . $node->nid);

  // Return the path.
  return $path;
}


/**
 * Create an ActionKit Petition.
 */
function actionkit_create_petition($title, $statement_leadin, $statement_text, $about_text, $thank_you_text, &$slug) {

  $server = url('https://' . variable_get('actionkit_username') . ':' . variable_get('actionkit_password') . '@' . variable_get('actionkit_server') . '/api', array('external' => TRUE));

  // Try to create the page.
  $page_id = actionkit_create_page($server, "PetitionPage", $title, $slug);

  // If we run out numbers to append something went wrong.
  if ($page_id == -1) {
    return $page_id;
  }

  if ($page_id == -2) {
    return $page_id;
  }

  // Create the petition text.
  $options = array(
    'PetitionForm.save_or_create' => array(
      (object) array(
        "page_id" => (int) $page_id,
        "statement_leadin" => (string) $statement_leadin,
        "statement_text" => (string) $statement_text,
        "about_text" => (string) $about_text,
        "thank_you_text" => (string) $thank_you_text,
      ),
    ),
  );

  $result = xmlrpc($server, $options);

  // If our query did not complete something went wrong.
  if ($result == FALSE) {
    drupal_set_message(t("ERROR: @var"), array('@var' => xmlrpc_error_msg()), 'status');
  }

  return $page_id;

}

/**
 * Create Petition Page.
 *
 * THIS IS PROBABLY NOT USED ANYMORE.
 */
function actionkit_petition_create_page($server, $title, &$slug) {

  // Turn our title into a friendly slug.
  $name = actionkit_friendly_url($title);

  // Loop until we make the page.
  for ($i = 0; $i <= 99; $i++) {

    // Create the page array.
    $options = array(
      'PetitionPage.create' => array(
        (object) array(
          "title" => (string) $title,
          "name" => (string) $name,
        ),
      ),
    );

    // Send our query.
    $result = xmlrpc($server, $options);

    // Check the result of our query, if false it failed.
    if ($result == FALSE) {

      // Failed because the slug already exists.
      if (strpos(xmlrpc_error_msg(), '1062') !== FALSE) {
        $name = actionkit_friendly_url($title) . "_" . $i;
      }
      // If not we have a problem and should probably stop.
      else {
        drupal_set_message(t("ERROR: @var"), array('@var' => xmlrpc_error_msg()), 'status');
        // Return to the calling function with a bogus page ID.
        return -2;
      }
      // It didn't fail, so we made our page.
    }

    else {

      // Save the slug for node creation.
      $slug = $name;

      // Return the page ID for future steps.
      return $result['id'];

    }
  }

  // Leave the loop if we go through all 99 attempts. Return an error.
  return -1;
}

/**
 * Get the most recent signatures.
 */
function actionkit_petition_last_signatures_signatures($page_id) {

  // This is the query to run.
  $query = '/rest/v1/report/run/' . variable_get('actionkit_last_signatures_report') . '/?page_id=' . $page_id . "&refresh=true";

  // The full URL.
  $query_url = 'https://' . variable_get('actionkit_username') . ':' . variable_get('actionkit_password') . '@' . variable_get('actionkit_server') . $query;

  // Get the result.
  $result = json_decode(file_get_contents($query_url));

  // We only want one comment per person, this will pull in the most recent.
  $data = array();

  // Was the result empty?
  if (count($result) == 0) {
    return $data;
  }

  // We need a way to know if a name was already in our list of comments.
  $in_array = FALSE;

  // The first result is always one we want to keep, so just add it.
  array_push($data, $result[0]);

  // Loop through our array of comments, but stop once we have 5.
  for ($i = 1; $i < count($result) && count($data) < 5; $i++) {

    // We need to loop through the array of data we already have on each line.
    for ($j = 0; $j < count($data); $j++) {

      // Check if we have name from comment, and the name in the array match.
      if (in_array($result[$i][0], $data[$j]) && in_array($result[$i][1], $data[$j])) {

        // If they do, then we don't want this comment.
        $in_array = TRUE;
      }
    }

    // Check if we want to add this comment to our comment list.
    if (!$in_array) {
      array_push($data, $result[$i]);
    }

    else {
      $in_array = FALSE;
    }
  }

  // Return the comment array.
  return $data;
}


/**
 * Gets the number of signatures of a petition.
 */
function actionkit_petition_signature_count($page_id) {

  // This is the query to run.
  $query = '/rest/v1/report/run/sql/?query=' . urlencode("select count(distinct user_id) from core_action a where page_id=" . $page_id . " and status = 'complete' and created_at > {last_run} and " .
    "not ({partial_run} and exists(select * from core_action where user_id=a.user_id and page_id=" . $page_id . " and status = 'complete' and created_at < a.created_at));");

  $query = '/rest/v1/report/run/' . variable_get('actionkit_signature_report') . '/?page_id=' . $page_id . "&refresh=true";

  // The full URL.
  $query_url = 'https://' . variable_get('actionkit_username') . ':' . variable_get('actionkit_password') . '@' . variable_get('actionkit_server') . $query;

  // Get the result.
  $result = file_get_contents($query_url);

  // Remove all [ and ].
  $replacement_pattern = array("[", "]");

  $result = str_replace($replacement_pattern, "", $result);

  return $result;
}


/**
 * Determine if the current user has taken action on this page.
 */
function actionkit_petition_action_taken($page_id) {
  // We'll need access to the current user.
  global $user;

  // Load our report.
  $query = '/rest/v1/report/run/' . variable_get('actionkit_action_taken_report') . '/?page_id=' . $page_id . "&email=" . $user->mail . "&refresh=true";

  $query_url = 'https://' . variable_get('actionkit_username') . ':' . variable_get('actionkit_password') . '@' . variable_get('actionkit_server') . $query;

  // Get the result.
  $result = file_get_contents($query_url);

  // Remove all [ and ].
  $replacement_pattern = array("[", "]");

  $result = str_replace($replacement_pattern, "", $result);

  return $result;
}


/**
 * Sign the petition in AK.
 */
function actionkit_petition_sign() {

  // Get the URL of the server.
  $server = 'https://' . variable_get('actionkit_username') . ':' . variable_get('actionkit_password') . '@' . variable_get('actionkit_server') . '/api';

  // Split the "Name" field into first and last name.
  $name_pieces = explode(" ", $_POST["name"], 2);

  // Build our action query, this will create a user if one doesn't exist in AK.
  $options = array(
    'act' => array(
      (object) array(
        "page" => $_POST["slug"],
        "first_name" => $name_pieces[0],
        "last_name" => $name_pieces[1],
        "email" => $_POST["email"],
        "zip" => $_POST["zip"],
      ),
    ),
  );

  // Send our XML RPC request.
  $result = xmlrpc($server, $options);

  // Add a comment.
  $options = array(
    "Action.set_custom_fields" => array(
      (object) array(
        "id" => $result["action"]["id"],
        "comment" => $_POST["comment"],
      ),
    ),
  );

  // Send our XML RPC request.
  $result = xmlrpc($server, $options);
}


/**
 * Wrapper function to have AK generate the list of signatures.
 */
function actionkit_generate_signatures() {
  // Get the download URL for the CSV.
  $url = actionkit_signature_list();

  // Get the CSV.
  actionkit_load_signatures($url);
}


/**
 * Query to get signature report and return URL to the completed query on AK.
 */
function actionkit_signature_list() {
  // Get the URL of the query as a background task.
  $server = 'https://' . variable_get('actionkit_username') . ':' . variable_get('actionkit_password') . '@' . variable_get('actionkit_server') .
    '/rest/v1/report/background/' . variable_get('actionkit_petition_signatures') . '/?format=csv';

  // Init cURL.
  $ch = curl_init($server);

  // Set up the data we need to send, and encode it as JSON.
  $data = array("page_id" => $_POST["page_id"]);
  $data_string = json_encode($data);

  // Set up our cURL options. THESE ARE ALL REQUIRED.
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_HEADER, 1);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Content-Length: ' . strlen($data_string))
  );

  // Get our response.
  $response = curl_exec($ch);

  // Get the HTTP Response Code.
  $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

  // Close our cURL resource.
  curl_close($ch);

  // If our response code isn't 201, we had some kind of an error.
  if ($code != 201) {
    return FALSE;
  }

  // Process our response to just get the header.
  $resp = explode("\n\r\n", $response);
  $header = explode("\n", $resp[0]);

  // Get the URL of the background task.
  $task_url = trim(substr($header[6], 18));

  // We need to track if the task is finished or not.
  $task_done = FALSE;

  // Loop while we're waiting for it to finish.
  while ($task_done == FALSE) {
    // Wait 5 Seconds before asking AK if the query is done.
    sleep(10);

    // Ask AK if the report is done.
    $task = json_decode(file_get_contents("https://" . variable_get('actionkit_username') . ':' . variable_get('actionkit_password') . "@" . $task_url), TRUE);

    // Check what AK said.
    if ($task['completed'] == TRUE) {
      $task_done = TRUE;
    }
  }

  // Formate the URL of the CSV file.
  $download_url = 'https://' . variable_get('actionkit_username') . ':' . variable_get('actionkit_password') . '@' . variable_get('actionkit_server') . $task['details']['download_uri'];

  // Print the download URL.
  return $download_url;
}


/**
 * Generate a local copy of the file.
 */
function actionkit_load_signatures($url) {

  // Send a Drupal HTTP request to get the CSV file.
  $csv = drupal_http_request($url);

  // Create a local drupal public CSV. Add random for security.
  $fileurl = "public://" . $_POST["page_id"] . "-" . rand(0, 1000000) . ".csv";
  $file = file_save_data($csv->data, $fileurl, FILE_EXISTS_REPLACE);

  // Check if the file saved correctly.
  if (!empty($file)) {

    // Create an out-side accesable URL.
    $url = file_create_url($file->uri);

    // Filter out the "/sites/all/modules/actionkit/php" Drupal adds.
    $url = str_replace("/sites/all/modules/actionkit/php", "", $url);

    // Output our URL so our JS can pick it up.
    echo $url;
  }
}
