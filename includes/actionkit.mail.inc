<?php
/**
 * @file
 * Email Functions for the actionkit module.
 */

/**
 * Set up an email message.
 */
function actionkit_mail($key, &$message, $params) {
  global $user;

  $options = array(
    'langcode' => $message['language']->language,
  );

  switch ($key) {
    case 'email_share':

      $message['subject'] = $params['subject'];

      // Note that the message body is an array, not a string.
      $message['body'][] = t('@name sent you the following message:', array('@name' => $user->name), $options);

      $message['body'][] = check_plain($params['message']);
      break;
  }
}

/**
 * Sends an Email.
 */
function actionkit_mail_send($type, $from, $email, $subject, $body, $path) {

  // All system mails need to specify the module and template key.
  $module = 'actionkit';
  $key = $type;

  // Specify 'to' and 'from' addresses.
  $to = $email;
  $from = $from;

  // Set the default language.
  $language = language_default();

  // Set our message parameters.
  $params['subject'] = $subject;
  $params['message'] = $body . "\n\n" . t('Fund this campaign at:') . " " . $path;

  // Send the mail as soon as the request is sent.
  $send = TRUE;

  // Send the mail.
  $result = drupal_mail($module, $key, $to, $language, $params, $from, $send);

  // Send back a success or failure message.
  if ($result['result'] == TRUE) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}


/**
 * Function to call when somoene clicks "Send Email!" on the email TAF form.
 */
function actionkit_email_share() {

  // A counter to track how many emails have been sent successfully.
  $sent = 0;

  // Set up an array to hold bad emails.
  $bad_emails = array();

  // Parse the CSV with the delimiter being a newline.
  $data = str_getcsv($_POST["emails"], "\n");

  // Loop through all of the emails in the array.
  for ($i = 0; $i < count($data); $i++) {

    // Trim off any extra space.
    $email = trim($data[$i]);

    // Try to email this address.
    $result = actionkit_mail_send("email_share", $_POST["from"], $email, $_POST["subject"], $_POST["body"], $_POST['path']);

    // Add this email to the bad email list if we had problems.
    if (!$result) {
      $bad_emails[] = $email;
      // Otherwise, increment our success counter.
    }
    else {
      $sent++;
    }
  }

  // Build our return data.
  $return = array();

  $return['subject'] = $_POST['subject'];
  $return['from'] = $_POST['from'];
  $return['body'] = $_POST['body'];

  $return['bad_emails'] = $bad_emails;
  $return['sent'] = $sent;
  $return['total'] = count($data);

  // Return the data.
  echo json_encode($return);
}
