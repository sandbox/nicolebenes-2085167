<?php
/**
 * Code specific to ActionKit donate pages.
 *
 * @author Developed by Richir Outreach | http://www.richiroutreach.com
 */

/**
 * Load the includes.
 */
module_load_include('inc', 'actionkit', 'includes/actionkit.user');
module_load_include('inc', 'actionkit', 'includes/actionkit.general');

/**
 * Create the form for creating a new donate page.
 */
function actionkit_donate_form($form, &$form_state) {
  global $language;

  // Get the default values for this form.
  $default_values = actionkit_default_values('donation');

  $form['name'] = array(
    '#type'    => 'textfield',
    '#title'    => t('Campaign Name'),
    '#maxlength'  => 255,
    '#description'  => t('Name your crowdfunding campaign. This should be short, but descriptive.'),
    '#required'  => TRUE,
  );

  $form['body'] = array(
    '#type'    => 'text_format',
    '#format'  => NULL,
    '#base_type'  => 'textarea',
    '#title'    => t('Donate Text'),
    '#description'  => t('Explain why people should donate to your cause.'),
    '#default_value' => $default_values['body'],
    '#required'  => TRUE,
  );

  $form['goal'] = array(
    '#label'    => t('goal'),
    '#type'    => 'textfield',
    '#title'    => t('Goal Amount (U.S. Dollars)'),
    '#description'  => t('Set a goal amount to raise.'),
    '#required'  => FALSE,
    '#field_parents' => array(),
    '#field_name' => 'goal',
    '#language' => $language->language,
    '#default_value' => $default_values['goal'],
  );

  $form['thank_you'] = array(
    '#type'    => 'text_format',
    '#format'  => NULL,
    '#base_type'  => 'textarea',
    '#title'    => t('Thank You Message'),
    '#description'  => t('This will be displayed after someone donates on your page.'),
    '#required'  => TRUE,
    '#default_value' => $default_values['thank_you'],
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value'  => t('Create campaign'),
  );

  return $form;
}


/**
 * Validation for creating a new petition.
 */
function actionkit_donate_form_validate($form, &$form_state) {
  if (actionkit_profanity_check($form_state['values']['name'])) {
    form_set_error('name', t('Your title cannot contain profanity.'));
  }

  if (actionkit_profanity_check($form_state['values']['body']['value'])) {
    form_set_error('body', t('Your text cannot contain profanity.'));
  }

  if (actionkit_profanity_check($form_state['values']['thank_you']['value'])) {
    form_set_error('thank_you', t('Your thank you message cannot contain profanity.'));
  }

  // We only need to validate if they put in a value.
  if ($form_state['values']['goal']) {
    // Make sure it's a valid dollar amount (ie xxx.xx).
    if (!preg_match("/^([1-9][0-9]*|0)(\.[0-9]{2})?$/", $form_state['values']['goal'])) {
      form_set_error('goal', t('Please enter a valid dollar amount without the dollar sign.'));
    }

    // $10 minimum, $1,000,000 maximum.
    if ($form_state['values']['goal'] > 1000000 || $form_state['values']['goal'] < 10) {
      form_set_error('goal', t('Please enter a goal between 10 and 1000000.'));
    }
  }
}


/**
 * Implements hook_submit().
 */
function actionkit_donate_form_submit($form, &$form_state) {
  global $user;
  global $base_url;

  // Slug is unused at this moment but we still need it.
  $slug = "";

  // Was this petition created by someone not logged in?
  if ($user->uid == 0) {

    // Store the form values in some session variables.
    $_SESSION['actionkit_donate_name'] = $form_state['values']['name'];
    $_SESSION['actionkit_donate_body'] = $form_state['values']['body'];
    $_SESSION['actionkit_donate_thank_you'] = $form_state['values']['thank_you'];
    $_SESSION['actionkit_donate_goal'] = $form_state['values']['goal'];
    $_SESSION['actionkit_source'] = 'donate';
    $form_state['redirect'] = "new/account";
  }
  // We can make the petition since this person is already logged in.
  else {

    // Try to make our petition and get the page ID.
    $page_id = actionkit_create_donate($form_state['values']['name'], "This Donation form was created programmatically via Drupal. For information about this petition, please visit " . $base_url, $slug);

    // Only try to create a node if we made a petition.
    if ($page_id > 0) {

      // Create a node with the actionkit data.
      $redirect = actionkit_donate_node($form_state['values']['name'], $form_state['values']['body'], $form_state['values']['goal'], $form_state['values']['thank_you'], $page_id, $slug);

      // Load the user so we can access our custom fields.
      $user_fields = user_load($user->uid);

      // Get our First and Last names.
      $first = field_get_items("user", $user_fields, "user_firstname");
      $last = field_get_items("user", $user_fields, "user_lastname");

      // Create / Update this user in ActionKit.
      actionkit_create_user($user_fields->mail, $first[0]['value'], $last[0]['value']);

      // Sign a petition to tag the user as creating a donate page.
      actionkit_dummy_sign("Donate");
    }

    $form_state['redirect'] = $redirect;
  }
}


/**
 * Create an ActionKit Petition.
 */
function actionkit_create_donate($title, $ask, &$slug) {

  $server = url('https://' . variable_get('actionkit_username') . ':' . variable_get('actionkit_password') . '@' . variable_get('actionkit_server') . '/api', array('external' => TRUE));

  // Try to create the page.
  $page_id = actionkit_create_page($server, 'DonationPage', $title, $slug);

  // Did we run out numbers to append?
  if ($page_id == -1) {
    // Echo "ERROR: We tried 99 title variations and still failed.<br/>";
    return $page_id;
  }

  if ($page_id == -2) {
    // Echo "ERROR: Encountered an unknown error.<br/>";
    return $page_id;
  }

  // Create the Donation Page specific content.
  $options = array(
    'DonationPage.save' => array(
      (object) array(
        "id" => (int) $page_id,
        "payment_account" => (string) variable_get('actionkit_payment_account'),
        "allow_international" => TRUE,
      ),
    ),
  );

  $result = xmlrpc($server, $options);

  // Did our query complete?
  if ($result == FALSE) {
    drupal_set_message(t("ERROR: @var"), array('@var' => xmlrpc_error_msg()), 'status');
  }

  // Create the form specific content.
  $options = array(
    'DonationForm.save_or_create' => array(
      (object) array(
        "page_id" => (int) $page_id,
        "ask_text" => (string) $ask,
      ),
    ),
  );

  $result = xmlrpc($server, $options);

  // Did our query complete?
  if ($result == FALSE) {
    drupal_set_message(t("ERROR: @var"), array('@var' => xmlrpc_error_msg()), 'status');
  }

  // Add After Action Email.
  // PHP isn't happy with ${{ in the string, but splitting it up worked.
  $options = array(
    'PageFollowup.create' => array(
      (object) array(
        "page_id" => $page_id,
        "email_body" => variable_get('actionkit_receipt_body'),
        "email_subject" => variable_get('actionkit_receipt_subject'),
      ),
    ),
  );

  $result = xmlrpc($server, $options);

  // Did our query complete?
  if ($result == FALSE) {
    drupal_set_message(t("ERROR: @var"), array('@var' => xmlrpc_error_msg()), 'status');
  }

  return $page_id;
}


/**
 * Make a node of type actionkit_donation.
 *
 * @title Title of the node to create.
 * @page_id Page ID of the actionkit petition.
 * @slug Slug of the actionkit petition.
 */
function actionkit_donate_node($name, $ask, $goal, $thank_you, $page_id, $slug) {
  global $user;

  // Create a generic object.
  $node = new stdClass();

  // Node Title.
  $node->title = $name;
  // Content Type of Node.
  $node->type = 'actionkit_donation';
  // 1 = Published.
  $node->status = 1;
  // 0 = Not Promoted to Front Page.
  $node->promote = 0;
  // 0 = No revision.
  $node->revision = 0;
  // Creators's user ID.
  $node->uid = $user->uid;
  // Creator's user name.
  $node->name = $user->name;
  // English.
  $node->language = 'en';
  // 0 = No comments.
  $node->comment = 0;
  // Node creation time.
  $node->created = $_SERVER['REQUEST_TIME'];
  // Node last edited changed time.
  $node->changed = $_SERVER['REQUEST_TIME'];
  $node->body[LANGUAGE_NONE][0] = $ask;
  // Set the page ID.
  $node->actionkit_page_id[LANGUAGE_NONE][0]['value'] = $page_id;
  // Set the slug.
  $node->actionkit_slug[LANGUAGE_NONE][0]['value'] = $slug;
  // Set the thank you message.
  $node->actionkit_thank_you[LANGUAGE_NONE][0] = $thank_you;
  if ($goal == NULL) {
    // Set the goal to -1 if not entered.
    $node->actionkit_donation_goal[LANGUAGE_NONE][0]['value'] = -1;
  }

  else {
    // Set the goal.
    $node->actionkit_donation_goal[LANGUAGE_NONE][0]['value'] = $goal;
  }

  // Submit and Save the node.
  node_submit($node);
  node_save($node);

  // Find our the path of the node we created, its alias if possible.
  $path = drupal_get_path_alias("node/" . $node->nid);

  // Finally, return the path.
  return $path;
}


/**
 * Implements hook_amount_raised().
 */
function actionkit_amount_raised($page_id) {

  // Load our report.
  $query = '/rest/v1/report/run/' . variable_get('actionkit_total_donation_amount') . '/?page_id=' . $page_id . "&refresh=true";

  $query_url = 'https://' . variable_get('actionkit_username') . ':' . variable_get('actionkit_password') . '@' . variable_get('actionkit_server') . $query;

  // Get the result.
  $result = file_get_contents($query_url);

  // Remove all [ and ].
  $replacement_pattern = array("[", "]");

  $result = str_replace($replacement_pattern, "", $result);

  if ($result <= 0) {
    return "0";
  }

  // Since this is a dollar amount, formate it correctly.
  return money_format('%i', $result);
}

/**
 * Implements hook_process_donation().
 */
function actionkit_process_donation() {
  // Get the URL of the server.
  $server = 'https://' . variable_get('actionkit_username') . ':' . variable_get('actionkit_password') . '@' . variable_get('actionkit_server') . '/api';

  // Split the "Name" field into first and last name.
  $name_pieces = explode(" ", $_POST["name"], 2);

  $options = array(
    'act' => array(
      (object) array(
        "page" => $_POST["slug"],
        "first_name" => $name_pieces[0],
        "last_name" => $name_pieces[1],
        "email" => $_POST["email"],
        "zip" => $_POST["zip"],
        "address1" => $_POST["address1"],
        "city"  => $_POST["city"],
        "state"  => $_POST["state"],
        "country"  => $_POST["country"],
        "region"  => $_POST["region"],
        "postal"  => $_POST["postal"],
        "amount"  => $_POST["amount"],
        "exp_date_month" => $_POST["exp_date_month"],
        "exp_date_year" => $_POST["exp_date_year"],
        "card_num" => $_POST["card_num"],
      ),
    ),
  );

  $result = xmlrpc($server, $options);

  echo json_encode($result);
}
