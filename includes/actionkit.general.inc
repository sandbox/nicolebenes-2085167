<?php
/**
 * Code for handling general AK related functions for the ActionKit Module.
 *
 * @author Developed by Richir Outreach | http://www.richiroutreach.com
 */

/**
 * Sign the "Dummy" Petitions used to tag people.
 *
 * @source: Where we're coming from.
 */
function actionkit_dummy_sign($source) {
  // We'll need access to the user for information.
  global $user;

  // Load user fields we'll need.
  $user_fields = user_load($user->uid);

  // Set up our server URL.
  $server = url('https://' . variable_get('actionkit_username') . ':' . variable_get('actionkit_password') . '@' . variable_get('actionkit_server') . '/api', array('external' => TRUE));

  // Determine what slug to sign.
  if ($source == "Petition") {
    $slug = variable_get('actionkit_petition_slug');
  }

  elseif ($source == "Donate") {
    $slug = variable_get('actionkit_donate_slug');
  }

  else {
    $slug = "";
  }

  // Get any information from the AK user we can.
  $options = array(
    'User.get' => array(
      (object) array(
        "email" => $user->mail,
      ),
    ),
  );

  // Send AK our query.
  $result = xmlrpc($server, $options);

  // If no first name, check their Drupal profile. Otherwise use name from AK.
  if ($result['first_name'] == "") {
    $first = field_get_items("user", $user_fields, "user_firstname");
    $first = $first[0]['value'];
  }

  else {
    $first = $result['first_name'];
  }

  // If no last name, check their Drupal profile. Otherwise use name from AK.
  if ($result['last_name'] == "") {
    $last = field_get_items("user", $user_fields, "user_lastname");
    $last = $last[0]['value'];
  }

  else {
    $last = $result['last_name'];
  }

  // If they don't have a zip code, put in a dummy one.
  if ($result['zip'] == "") {
    $zip = "99999";
  }

  else {
    $zip = $result['zip'];
  }

  // Sign the appropriate petition.
  $options = array(
    'act' => array(
      (object) array(
        "page" => $slug,
        "first_name" => $first,
        "last_name" => $last,
        "email" => $user->mail,
        "zip" => $zip,
      ),
    ),
  );

  // Send our query to AK.
  $result = xmlrpc($server, $options);

  // Did our query complete?
  if ($result == FALSE) {
    drupal_set_message(t("ERROR: @var"), array('@var' => xmlrpc_error_msg()), 'status');
  }
}


/**
 * Create Petition Page.
 *
 * @server: url object that points to the AK server
 * @page_type: The type of page to create
 * @title: The title of the page
 * @slug: The slug we end up creating
 */
function actionkit_create_page($server, $page_type, $title, &$slug) {

  // Turn our title into a friendly slug.
  $name = actionkit_friendly_url($title);

  // Loop until we make the page or try 99 times.
  for ($i = 0; $i <= 99; $i++) {

    // Create the page array.
    $options = array(
      $page_type . '.create' => array(
        (object) array(
          "title" => (string) $title,
          "name" => (string) $name,
        ),
      ),
    );

    // Send our query.
    $result = xmlrpc($server, $options);

    // Check the result of our query, if false it failed.
    if ($result == FALSE) {

      // Did it fail because the slug already exists?
      if (strpos(xmlrpc_error_msg(), '1062') !== FALSE) {
        // Add a number to the end!
        $name = actionkit_friendly_url($title) . "_" . $i;
      }

      // If not we have a problem and should probably stop.
      else {
        drupal_set_message(t("ERROR: @var"), array('@var' => xmlrpc_error_msg()), 'status');

        // Return to the calling function with a bogus page ID.
        return -2;
      }

      // It didn't fail, so we made our page!
    }

    else {

      // Save the slug for node creation.
      $slug = $name;

      // For reasons only AK knows, we can't set the tag when
      // we call the create function. Also hide the page in AK
      // so it doesn't clutter up the page display.
      $options = array(
        $page_type . '.save' => array(
          (object) array(
            "id" => $result['id'],
            "hidden" => 1,
            "tags" => array(
              (object) array(
                "id" => (int) variable_get('actionkit_page_tag_id'),
              ),
            ),
          ),
        ),
      );

      // Call our query again.
      $result = xmlrpc($server, $options);

      // Test if our query completed.
      if ($result == FALSE) {
        drupal_set_message(t("ERROR: @var"), array('@var' => xmlrpc_error_msg()), 'status');
      }

      // Return the page ID for future steps.
      return $result['id'];

    }
  }

  // We only leave the for loop and reach this if we went through all
  // 99 page creation attempts - ouch! Return an error value.
  return -1;
}


/**
 * This function simply tests for bad words.
 */
function actionkit_profanity_check($message) {

  // If the message is empty, we don't need to test.
  if (strlen($message) <= 1) {
    return FALSE;
  }

  // Remove any HTML.
  $message = strip_tags($message);

  // These are the bad words to filter out.
  $badwords = array(
    "nigger", "spic", "wetback", "beaner", "nazi", "faggot",
    "cunt", "bitch", "fuck", "shit", "cock", "ass", "asshole",
    "porno", "porn", "pussy", "dick", "fag", "SEO",
    "viagra", "douche", "damn", "whore",
    "retard",
  );

  // We'll add these to the words to look for profanity.
  $suffixes = array(
    "", "s", "es", "'s", "ing", "bag", "it", "er", "face",
  );

  // Break the string into tokens.
  $sub_msg = strtok($message, " .");

  // While we have another word (token), keep working.
  while ($sub_msg !== FALSE) {

    // Make sure no whitespace ruins our day.
    $sub_msg = trim($sub_msg);

    // Go through our list of words.
    foreach ($badwords as $word) {

      // Go through our list of Suffixes.
      foreach ($suffixes as $suffix) {

        // If word + suffix is the same as our word, we've found a naughty word.
        if (strtolower($sub_msg) == ($word . $suffix)) {
          // We can just jump out of the loop since we found a bad word.
          return TRUE;
        }
      }
    }

    // Last word passed, so move on to the next.
    $sub_msg = strtok(" .");
  }

  // Only reach this point if we had no bad words.
  return FALSE;
}


/**
 * Generate the default fields for a given form type.
 */
function actionkit_default_values($type) {

  // Set up an array to hold our return values.
  $return = array();

  // Body is universal to all content types.
  $field = field_info_field('body');
  $instance = field_info_instance('node', 'body', 'actionkit_' . $type);
  $default = field_get_default_value('node', NULL, $field, $instance, NULL);

  if (array_key_exists(0, $default)) {
    $return['body'] = $default[0]['value'];
  }
  else {
    $return['body'] = "";
  }

  // Thank you is also a gloabl field.
  $field = field_info_field('actionkit_thank_you');
  $instance = field_info_instance('node', 'actionkit_thank_you', 'actionkit_' . $type);
  $default = field_get_default_value('node', NULL, $field, $instance, NULL);

  if (array_key_exists(0, $default)) {
    $return['thank_you'] = $default[0]['value'];
  }
  else {
    $return['thank_you'] = "";
  }

  switch ($type) {
    case 'donation':
      // This is our goal.
      $field = field_info_field('actionkit_donation_goal');
      $instance = field_info_instance('node', 'actionkit_donation_goal', 'actionkit_donation');
      $default = field_get_default_value('node', NULL, $field, $instance, NULL);

      if (array_key_exists(0, $default)) {
        $return['goal'] = $default[0]['value'];
      }
      else {
        $return['goal'] = "";
      }

      break;

    case 'petition':
      // This is our target.
      $field = field_info_field('actionkit_petition_target');
      $instance = field_info_instance('node', 'actionkit_petition_target', 'actionkit_petition');
      $default = field_get_default_value('node', NULL, $field, $instance, NULL);

      if (array_key_exists(0, $default)) {
        $return['target'] = $default[0]['value'];
      }
      else {
        $return['target'] = "";
      }

      // The About Text.
      $field = field_info_field('actionkit_petition_about');
      $instance = field_info_instance('node', 'actionkit_petition_about', 'actionkit_petition');
      $default = field_get_default_value('node', NULL, $field, $instance, NULL);

      if (array_key_exists(0, $default)) {
        $return['about'] = $default[0]['value'];
      }
      else {
        $return['about'] = "";
      }

      break;
  }

  return $return;
}


/**
 * Turns the string into a slug. From: http://neo22s.com/slug/.
 */
function actionkit_friendly_url($url) {
  // Everything to lower and no spaces begin or end.
  $url = strtolower(trim($url));

  // Adding - for spaces and union characters.
  $find = array(' ', '&', '\r\n', '\n', '+', ',');
  $url = str_replace($find, '-', $url);

  // Delete and replace rest of special chars.
  $find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
  $repl = array('', '_', '');
  $url = preg_replace($find, $repl, $url);

  // Return the friendly url.
  return $url;
}
