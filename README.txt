ActionKit Module
Created by Richir Outreach
http://www.richiroutreach.com
-----------------------------

I. Overview

The goal of the ActionKit module is to allow ordinary users to use some the 
features of ActionKit through Drupal. The current feature list is:

	* Users can create ActionKit petition pages and donate pages
	* Donate pages function exactly as if they were created through 
		ActionKit, but without ever leaving the Drupal site
	* Petition pages can be signed by the visitors to the site
	* Pages allow for an image to be attached
	* Petition pages allow for signature list to be downloaded by user


II. Installition Instructions

See INSTALL.TXT


III. Detailed Description

This module uses the ActionKit XML-RPC server to communicate requests 
and accept replies to integrate Drupal with ActionKit. The initial goal 
of the project was to utilize ActionKit and Drupal together as a micro 
version of sites such as change.org. To this end, the module allows for 
easy user created petition and donation pages. This allows for site 
users to create their own petitions and donation pages within the 
community they are using, without having to go to an outside third
party. Allowing members to expand their involvement in the community
and issue by creating their own campaigns, increases their investment 
and also helps to spread issues better. Petitions and donation pages
are the first pages to be supported, since those are the two most 
useful for the goals of this site.

Another goal of the project is to maintain all the great data gathering 
ActionKit does for us. Using a user created petition or donation page is 
exactly the same as using one on ActionKit's server, so there is zero 
loss of useful information. 

IV. How to create a petition and donation page

Navigate to /new/petition to create a petition.

Navigation to /new/donation to create a donation page.

V. Future Features
