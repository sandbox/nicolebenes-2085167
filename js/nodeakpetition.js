(function ($) {
  $(document).ready(function() {

    // Hide the thank you text.
    $('#thank-you').hide();

    // Set up form validation.
    $('#sign-petition-form').validate();

    // Process clicking the form.
    $('#sign-petition-form').submit(function(e) {

      // Don't submit the form.
      e.preventDefault();

      // Ensure the form passed validation.
      if($(this).valid()) {

        // Call our PHP helper with all of our needed data.
        $.post('/ajax/sign', { slug: $("#slug").val(), name: $("#name").val(), email: $("#email").val(), zip: $("#zip").val(), comment: $('#comment').val() /*, password: $('#password') */} , function(data) {

          $('#thank-you').fadeIn(400);
          $('#petition').fadeOut(400);
        });
      }
    });

    // Refresh page once thank-you is closed.
    $('#thank-you .reload').click(function(e) {
      e.preventDefault();
      document.location.reload(true);
    });

    // Download signatures button.
    $('#download-signatures').click(function(e) {

      // Change the button to indicate something is happening and make it disabled.
      $('#download-signatures').attr('disabled','disabled').val('Fetching File...');

       // Call our PHP Helper.
      $.post('/ajax/signatures', { page_id: actionkit_ak_page_id }, function(data) {

        if(data != false) {

          // Redirect to the download link.
          window.location = data;

          $('#download-signatures').val('Download Started');

        } else {
          // We had some kind of an error.
          $('#download-signatures').removeAttr('disabled','disabled').val('There seems to have been an error. Click to retry.');
        }
      });
    });
  });
}) (jQuery);

function actionkit_newWindow(height,width) {
  var settings = "height=" + height + ",width=" + width + ",status=yes,toolbar=no,menubar=no,location=no";
  window.open("about:blank","actionkit_newwindow", settings);
}
