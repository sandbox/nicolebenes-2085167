(function ($) {
  $(document).ready(function() {

    // Hide the international fields since we start on the US
    $('#intl_billing_fields').hide();

    // Hide our success box
    $('#success').hide();

    // Hide the Thermometer
    $('#meter').hide();

    // Clear any errors on the card number box when we focus it
    $('#card_num').focus(function() {
      $(this).val("").css("border-color", "#ccc").css("background-color", "#fff");
    });

    // Set up form validation
    $('#donate-form').validate();

    $('#donate-form').submit(function(e) {
      // Don't submit the form
      e.preventDefault();

      // Ensure the form passed validation
      if($(this).valid()) {

        // change the button & disable it
        $('#submitbutton').attr('disabled','disabled').text('Processing...').addClass('secondary');

        // Call our PHP helper with all of our needed data
        $.post('/ajax/donate', { slug: $("#slug").val(), name: $("#name").val(), email: $("#email").val(), zip: $("#zip").val(), address1: $("#address1").val(), city: $("#city").val(),
            state: $("#id_state").val(), country: $("#country").val(), region: $("#region").val(), postal: $("#postal").val(), amount: $("#donation_amount").val(),
            exp_date_month: $("#exp_date_month").val(), exp_date_year: $("#exp_date_year").val(), card_num: $("#card_num").val() }, function(data) {

          var result = jQuery.parseJSON(data);

          $("#processing").fadeOut();
          $("#processing-shadow").fadeOut();

          // Donation was successful
          if(result['status'] == 'success') {
            $('#donate-page').hide();
            $('#success').show();

          } else if (result['status'] == 'processing_error') {
            $('#card_num').val("Your card was declined.").css("border-color", "#c60f13").css("background-color", "rgba(198, 15, 19, 0.1)");
            $('#submitbutton').removeAttr('disabled').removeClass('secondary');

          } else if (result['status'] == 'invalid_input') {
            if(result['errors']['amount:minimum']) {
              window.scrollTo(0,0);
              $('#form-errors').append('<div class="error-message">' + result['errors']['amount:minimum'] + '</div>');
            } else {
              $('#card_num').val("Invalid Card Number.").css("border-color", "#c60f13").css("background-color", "rgba(198, 15, 19, 0.1)");
            }
              $('#submitbutton').removeAttr('disabled').removeClass('secondary');
          }
        });
      }
    });

    // reload
    $('.reload').click(function(e) {
      e.preventDefault();
      document.location.reload(true);
    });

    // Show goal if one is set
    if(actionkit_goal > 0) {

      // Show the Thermometer
      $('#meter').show();
    }

    // Fixes for the theme
    $('.amount-select').click(function() {
      clear_other();
      update_total();
    });

    $('.amount-select').change(function() {
      update_total();
    });

    $('.amount-select').blur(function() {
      update_total();
    });

    $('#amount_other_field').blur(function() {
      clear_radio_buttons();
      update_total();
    });

    $('#amount_other_field').click(function() {
      clear_radio_buttons();
      update_total();
    });

    $('#country').change(function() {
      country_change();
    });

    $('#country').blur(function() {
      country_change();
    });

    // Send Email Button (Popup)
    $("#email-share").submit(function(e) {

      // Don't actually submit the form
      e.preventDefault();

      // Change the button so people can't click it and spam
      $('#email-button').val("Sending...").attr("disabled", "disabled");

      // Did they enter a subject?
      if ($('#email-subject').val() != "") {
        var userSubject = $('#email-subject').val();
      } else {
        var userSubject = actionkit_page_title;
      }

      // Call our PHP helper with all of our needed data
      $.post('/ajax/email', { emails: $('#email-list').val(), from: $('#email-from').val(), body: $('#email-body').val(), path: actionkit_mail_path, subject: userSubject }, function(data) {

        $('#email-button').val("Sent!");

        var result = jQuery.parseJSON(data);
      });
    });

    // Send Email Button (TAF)
    $("#email-share-taf").submit(function(e) {

      // Don't actually submit the form
      e.preventDefault();

      // Change the button so people can't click it and spam
      $('#email-taf-button').val("Sending...").attr("disabled", "disabled");

      // Did they enter a subject?
      if ($('#email-subject-taf').val() != "") {
        var userSubject = $('#email-subject-taf').val();
      } else {
        var userSubject = actionkit_page_title;
      }

      // Call our PHP helper with all of our needed data
      $.post('/ajax/email', { emails: $('#email-list-taf').val(), from: $('#email-from-taf').val(), body: $('#email-body-taf').val(), path: actionkit_mail_path, subject: userSubject }, function(data) {

        $('#email-taf-button').val("Sent!");

        var result = jQuery.parseJSON(data);

      });
    });

    // Override styling and make the box appear farther up the page
    $('#email-popup').css("top", "-6em");

    // Share via Email button
    $('#email-share-button').click(function() {
      $('#email-popup').foundation('reveal', 'open');
    });

    // Popup Close Button
    $('#email-popup-close').click(function() {
      $('#email-popup').foundation('reveal', 'close');
    });

    // Set the "From" line automatically (if possible)
    $('#email-from').val(actionkit_mail);
    $('#email-from-taf').val(actionkit_mail);

    $('#country').blur(function() {
      country_change();
    });

    $('#country').change(function() {
      country_change();
    });

    // Helper Functions
    function country_change() {
       country = $('#country').val();
       if (country == 'United States') {
        $('#us_billing_fields').show();
        $('#intl_billing_fields').hide();
       } else {
        $('#us_billing_fields').hide();
        $('#intl_billing_fields').show();
       }
    }

    function clear_radio_buttons() {
       $('input.amount-select').attr('checked', false);
    }

    function clear_other() {
       $('#amount_other_field').val("");
    }

    function update_total() {
      var amount_checked = $("input:radio[name=amount]:checked").val();
      var total = 0;

      if (amount_checked) {
        total += parseFloat(amount_checked);
      } else {
        // or other amounts
        var other = $('#amount_other_field').val();
        if (other && /^[0-9]\d*(\.\d{2})?$/.test(other)) {
          total += parseFloat(other);
        }
      }

      var new_total = total == 0 ? "$0" : "$" + total.toFixed(2);
      if (new_total != $('#total').html()) {
        $('#donation_amount').val(new_total);
      }
    }
  });
}) (jQuery);

function newWindow(height,width) {
  var settings = "height=" + height + ",width=" + width + ",status=yes,toolbar=no,menubar=no,location=no";
  window.open("about:blank","newwindow", settings);
}
