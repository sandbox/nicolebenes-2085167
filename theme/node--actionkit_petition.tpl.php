<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */

// Determine if the current viewer owns this node and/or has signed it.
 global $user;

// Variable to hold if node is owned by current user.
$owner = FALSE;

// Set up some variables.
$status = 0;
$user_name = NULL;
$mail = NULL;
$user_zip = "";

// Check if the current user is anonymous.
if(!user_is_anonymous()):
  // Is the user the owner of this node?
  if ($user->name == $node->name):
    $owner = TRUE;
  endif;

  if($content['action_taken'] > 0):
    $status = 1;
  else:
    $status = 0;
  endif;

  // Even if not the owner, we can get some userful information from the user.
  $mail = $user->mail;

  // Load the user so we can access our custom fields.
  $user_fields = user_load($user->uid);

  // Get our First and Last names.
  $first = field_get_items("user", $user_fields, "user_firstname");
  $last = field_get_items("user", $user_fields, "user_lastname");
  $zip = field_get_items("user", $user_fields, "user_zip");

  $user_name = $first[0]['value'] . " " . $last[0]['value'];

  // Get the user Zip Code.
  if($zip == NULL):
    $user_zip = "";
  else:
    $user_zip = $zip[0]['value'];
  endif;
 endif;

$page_path = rawurlencode($GLOBALS['base_url']) . '/' . rawurlencode(drupal_get_path_alias('node/' . $node->nid));

$raw_path = $GLOBALS['base_url'] . '/node/' . $node->nid;

$twitter_message = substr($title, 0, 120) . " " . $page_path;

?>

<script>
  /*
    Declare variables we'll use in our external JS
  */
  var actionkit_module_path = '/<?php echo drupal_get_path('module', 'actionkit'); ?>';
  var actionkit_ak_page_id = <?php print $node->actionkit_page_id[LANGUAGE_NONE][0]['value']; ?>
</script>

<!-- TODO: use drupal jQuery -->
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<?php
drupal_add_js(drupal_get_path('module', 'actionkit') . '/js/nodeakpetition.js');
?>

<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div id="thank-you" class="alert-box success">
    <div class="thank-you-body">
      <?php print $node->actionkit_thank_you[LANGUAGE_NONE][0]['value']; ?>
    </div>
    <div class="share-page">
      <a href="http://www.facebook.com/share.php?u=<?php echo $page_path; ?>" onclick="actionkit_newWindow(275,475)" target="newwindow" class="facebook-button button large">Share on Facebook</a>
      <a href="https://twitter.com/intent/tweet?text=<?php echo $twitter_message; ?>" onclick="actionkit_newWindow(275,475)" target="newwindow" class="twitter-button button large">Tweet This!</a>
      <a href="#" class="back-to-petition reload button large alert">Back to Petition</a>
    </div>
  </div>
  <div id="petition">
    <div class="row">
      <h2 class="page-title"><?php print $title; ?></h2>
      <div class="six columns alert-box success">
        <p><span class="label">To: <?php print $node->actionkit_petition_target[LANGUAGE_NONE][0]['value']; ?></span></p>
        <div class="petition-body">
          <?php print render($content['body']); ?>
        </div> <!-- .petition-body -->
      </div>
      <div class="six columns">
        <?php if($owner): ?>
          <?php if(array_key_exists('field_image', $content)): ?>
            <a href="<?php echo $raw_path; ?>/edit" class="large alert button edit-button">Edit Your Campaign</a>
          <?php else: ?>
            <a href="<?php echo $raw_path; ?>/edit" class="large alert button edit-button">Add a Picture</a>
          <?php endif; ?>
          <input id="download-signatures" type="button" class="large alert button edit-button" value="Download Signatures">
        <?php endif; ?>
        <?php print render($content['field_image']); ?>
        <p><span class="label">This petition is important because:</span></p>
        <div class="petition-important">
          <?php print render($content['actionkit_petition_about']); ?>
          <div class="share-buttons">
            <a href="http://www.facebook.com/share.php?u=<?php echo $page_path; ?>" onclick="actionkit_newWindow(275,475)" target="newwindow" class="facebook-button-small button small">Share on Facebook</a>
            <a href="https://twitter.com/intent/tweet?text=<?php echo $twitter_message; ?>" onclick="actionkit_newWindow(275,475)" target="newwindow" class="twitter-button-small button small">Tweet This!</a>
          </div>
        </div> <!-- .petition-important -->
        <hr>
        <div class="petition-signers">

          <?php if($status == 0): ?>
            <?php if(render($content['signatures']) < 1): ?>
              <p><span class="label secondary">Be the first to sign this petition!</span></p>
            <?php else: ?>
              <p><span class="label secondary">Join the <?php print render($content['signatures']); ?> <?php if(render($content['signatures']) == 1): echo "person who supports"; else: echo "people who support"; endif;?> this!</span></p>
            <?php endif; ?>
          <?php endif; ?>

          <?php if($status == 1): ?>
            <?php if(render($content['signatures']) < 1): ?>
              <p><span class="secondary label">Be the first to sign this petition!</span></p>
            <?php else: ?>
              <p><span class="success label">You and <?php print (render($content['signatures']) - 1); ?> other <?php if(render($content['signatures']) == 2): echo "person"; else: echo "people"; endif; ?> support this!</span></p>
            <?php endif; ?>
            <div class="petition-comments">

              <table width="100%">
                <?php
                  for($i = 0; $i < count($content['last_signatures']); $i++):
                    echo "<tr><td class='signature-name'>" . $content['last_signatures'][$i]['first'] . " " . $content['last_signatures'][$i]['last'] . "</td><td class='signature-comment'>" . $content['last_signatures'][$i]['comment'] . "</td></tr>";
                  endfor;
                ?>
              </table>

            </div> <!-- .petition-comments -->
          <?php endif; ?>
          <div class="petition-sign">

            <?php if($status): ?>
              <h3>Update Signature</h3>
            <?php else: ?>
              <h3>Sign Petition</h3>
            <?php endif; ?>

            <form action="#" id="sign-petition-form" method="POST">
              <div class="form-item">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-text required" value="<?php if($user_name): echo $user_name; endif; ?>">
              </div> <!-- .form-item -->
              <div class="form-item">
                <label for="email">Email</label>
                <input type="text"  name="email" id="email" class="form-text required email" value="<?php if($mail): echo $mail; endif; ?>" <?php if($status == 1): echo "disabled='true'"; endif; ?>>
              </div> <!-- .form-item -->
              <div class="form-item">
                <label for="zip">Zip Code</label>
                <input type="text" class="required form-text zipcode" name="zip" id="zip" value="<?php echo $user_zip; ?>">
              </div>
              <div class="form-item">
                <label for="comment">Comment</label>
                <textarea name="comment" id="comment" rows="5" cols="25" class="form-textarea"></textarea>
              </div>
              <input type="hidden" name="slug" id="slug" value="<?php echo $node->actionkit_slug[LANGUAGE_NONE]['0']['value']; ?>">
              <input type="hidden" name="url" id="url" value="<?php echo $node_url; ?>">
              <button id="submitbutton" type="submit" class="button large donate-form-submit">Sign Petition</button>
            </form> <!-- #sign-petition-form -->
          </div>
        </div> <!-- .petition-signers -->
      </div> <!-- .six.columns -->
    </div> <!-- .row -->
  </div> <!-- #petition -->
</div> <!-- #node-X -->
