<?php

/**
 * @file
 * Default theme implementation to display a node.
 */

  // Determine if the current viewer owns this node and/or has signed it.
  global $user;
  $owner = FALSE;

  if (!user_is_anonymous()):
    if ($user->name == $node->name):
      $owner = TRUE;
    else:
      $owner = FALSE;
    endif;

    $mail = $user->mail;
  else:
    $mail = "";
  endif;

  $page_path = rawurlencode($GLOBALS['base_url']) . '/' . rawurlencode(drupal_get_path_alias('node/' . $node->nid));

  $twitter_message = substr($title, 0, 120) . " " . $page_path;

  $raw_path = $GLOBALS['base_url'] . '/node/' . $node->nid;

?>

<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<?php
  drupal_add_js(drupal_get_path('module', 'actionkit') . '/js/nodeakdonation.js');
?>

<script>
  var actionkit_module_path = "<?php echo drupal_get_path('module', 'actionkit'); ?>";

  var actionkit_goal = <?php echo render($content['goal']) ?>;
  var actionnkit_amount = <?php echo render($content['totalAmount']) ?>;

  var actionkit_page_title = "<?php echo $title; ?>";

  var actionkit_nid = <?php echo $node->nid ?>;

  // These were $mail_path and $mail in the JS, but seems to be no actual variables called that?
  var actionkit_mail_path = "<?php echo $GLOBALS['base_url'] . '/' . drupal_get_path_alias('node/' . $node->nid); ?>";
  var actionkit_mail = "<?php echo $mail ?>";
</script>

<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="row">
    <div class="twelve columns">
      <h2 class="page-title"><?php print $title; ?></h2>
      <div id="success" class="alert success">
        <div class="thank-you-body">
          <?php print $node->actionkit_thank_you[LANGUAGE_NONE][0]['value']; ?>
        </div>
        <div class="share-page">
          <a href="http://www.facebook.com/share.php?u=<?php echo $page_path; ?>" onclick="newWindow(275,475)" target="newwindow" class="facebook-button button large">Share on Facebook</a>
          <a href="https://twitter.com/intent/tweet?text=<?php echo $twitter_message; ?>" onclick="newWindow(275,475)" target="newwindow" class="twitter-button button large">Tweet This!</a>
          <a href="#" class="back-to-petition reload button large alert">Back to Campaign</a>
        </div>
      </div> <!-- #success -->
    </div>
    <div id="donate-page">
      <div class="six columns">
        <?php if($owner): ?>
          <?php if(array_key_exists('field_image', $content)): ?>
            <a href="<?php echo $raw_path; ?>/edit" class="large alert button edit-button">Edit Your Campaign</a>
          <?php else: ?>
            <a href="<?php echo $raw_path; ?>/edit" class="large alert button edit-button">Add a Picture</a>
          <?php endif; ?>
        <?php endif; ?>
        <div class="donate-image">
          <?php print render($content['field_image']); ?>
        </div> <!-- .donate-image -->
        <div class="donate-text">
          <?php print render($content['body']); ?>
        </div> <!-- .donate-text -->
      <?php if(render($content['totalAmount']) > 0): ?>
        <div id="meter">
          <?php
            $goal = render($content['goal']);
            $amount = render($content['totalAmount']);
            $percentnum = ($amount / $goal) * 100;
            $percent = $percentnum . '%';
          ?>
            <?php print $percent ?> Raised!
          <div id="mercury" style="width:<?php print $percent; ?>">
            <?php print $percent ?> Raised!
          </div>
        </div>
      <?php endif; ?>
        <?php if ($owner): ?>
          <div class="amount-raised">
            <?php if(render($content['totalAmount']) <= 0): ?>
              <p>You haven't raised any money, yet. <?php if(render($content['goal']) > 0): ?>Your goal is $<?php print render($content['goal']); endif; ?></p>
            <?php else: ?>
              <p>You have raised $<?php print render($content['totalAmount']); ?>! <?php if(render($content['goal']) > 0): ?>Your goal is $<?php print render($content['goal']); endif; ?>.</p>
            <?php endif; ?>
          </div>
        <?php else: ?>
          <div class="amount-raised">
            <?php if(render($content['totalAmount']) <= 0): ?>
              <p>This campaign hasn't raised any money yet, be the first and get it started!</p>
            <?php else: ?>
              <p>$<?php print render($content['totalAmount']); ?> raised so far! <?php if(render($content['goal']) > 0): ?>The goal for this campaign is $<?php print render($content['goal']); endif; ?>.</p>
            <?php endif; ?>
          </div>
        <?php endif; ?>
        <div class="share-buttons">
          <a href="http://www.facebook.com/share.php?u=<?php echo $page_path; ?>" onclick="newWindow(275,475)" target="newwindow" class="facebook-button-small button small new-window">Share on Facebook</a>
          <a href="https://twitter.com/intent/tweet?text=<?php echo $twitter_message; ?>" onclick="newWindow(275,475)" target="newwindow" class="twitter-button-small button small new-window">Tweet This!</a>
        </div>
      </div> <!-- .six.columns -->
      <div class="six columns">
        <form action="#" id="donate-form" method="POST">
          <div id="form-errors"></div>
          <input type="hidden" name="slug" id="slug" value="<?php echo $node->actionkit_slug[LANGUAGE_NONE]['0']['value']; ?>">
               <input type="hidden" name="url" id="url" value="<?php echo $node_url; ?>">
               <input type="hidden" name="donation_amount" id="donation_amount" value="">
          <div class="form-item">
            <label for="name">Name</label>
            <input id="name" type="text" name="name" class="form-text required">
          </div> <!-- .form-item -->
          <div class="form-item">
            <label for="email">Email</label>
            <input id="email" type="text" name="email" class="form-text required email">
          </div> <!-- .form-item -->
          <div class="form-item">
            <label for="address1">Billing address</label>
            <input type="text" id="address1" name="address1" class="form-text required">
          </div> <!-- .form-item -->
          <div class="form-item">
            <label for="city">City</label>
            <input type="text" id="city" name="city" class="form-text required">
          </div> <!-- .form-item -->
          <div id="us_billing_fields">
            <div class="form-item">
              <label for="id_state">State</label>
              <select name="state" id="id_state" class="form-select required">
                <option value="">Select</option>
                <option value="AL">Alabama</option>
                <option value="AK">Alaska</option>
                <option value="AZ">Arizona</option>
                <option value="AR">Arkansas</option>
                <option value="CA">California</option>
                <option value="CO">Colorado</option>
                <option value="CT">Connecticut</option>
                <option value="DE">Delaware</option>
                <option value="FL">Florida</option>
                <option value="GA">Georgia</option>
                <option value="HI">Hawaii</option>
                <option value="ID">Idaho</option>
                <option value="IL">Illinois</option>
                <option value="IN">Indiana</option>
                <option value="IA">Iowa</option>
                <option value="KS">Kansas</option>
                <option value="KY">Kentucky</option>
                <option value="LA">Louisiana</option>
                <option value="ME">Maine</option>
                <option value="MD">Maryland</option>
                <option value="MA">Massachusetts</option>
                <option value="MI">Michigan</option>
                <option value="MN">Minnesota</option>
                <option value="MS">Mississippi</option>
                <option value="MO">Missouri</option>
                <option value="MT">Montana</option>
                <option value="NE">Nebraska</option>
                <option value="NV">Nevada</option>
                <option value="NH">New Hampshire</option>
                <option value="NJ">New Jersey</option>
                <option value="NM">New Mexico</option>
                <option value="NY">New York</option>
                <option value="NC">North Carolina</option>
                <option value="ND">North Dakota</option>
                <option value="OH">Ohio</option>
                <option value="OK">Oklahoma</option>
                <option value="OR">Oregon</option>
                <option value="PA">Pennsylvania</option>
                <option value="RI">Rhode Island</option>
                <option value="SC">South Carolina</option>
                <option value="SD">South Dakota</option>
                <option value="TN">Tennessee</option>
                <option value="TX">Texas</option>
                <option value="UT">Utah</option>
                <option value="VT">Vermont</option>
                <option value="VA">Virginia</option>
                <option value="WA">Washington</option>
                <option value="DC">Washington, D.C.</option>
                <option value="WV">West Virginia</option>
                <option value="WI">Wisconsin</option>
                <option value="WY">Wyoming</option>
              </select> <!-- #id_state -->
            </div> <!-- .form-item -->
            <div class="form-item">
              <label for="zip">Zip Code</label>
              <input id="zip" type="text" name="zip" maxlength="5" size="5" class="form-text required">
            </div> <!-- .form-item -->
          </div> <!-- #us_billing_fields -->
          <div id="intl_billing_fields" class="ak-errs-below">
            <div class="form-item">
              <label for="region">Region</label>
              <input id="region"  type="text" name="region" size="20" class="required">
            </div> <!-- .form-item -->
            <div class="form-item">
              <label for="postal">Postal Code</label>
              <input id="postal"  type="text" name="postal" size="10" class="required">
            </div> <!-- .form-item -->
          </div> <!-- #intl_billing_fields -->
          <div class="form-item">
            <label for="country">Country</label>
            <select name="country" id="country" class="form-select required">
              <option selected value="United States">United States</option>
              <option value="Afghanistan">Afghanistan</option>
              <option value="Aland Islands">Aland Islands</option>
              <option value="Albania">Albania</option>
              <option value="Algeria">Algeria</option>
              <option value="American Samoa">American Samoa</option>
              <option value="Andorra">Andorra</option>
              <option value="Angola">Angola</option>
              <option value="Anguilla">Anguilla</option>
              <option value="Antarctica">Antarctica</option>
              <option value="Antigua and Barbuda">Antigua and Barbuda</option>
              <option value="Argentina">Argentina</option>
              <option value="Armenia">Armenia</option>
              <option value="Aruba">Aruba</option>
              <option value="Australia">Australia</option>
              <option value="Austria">Austria</option>
              <option value="Azerbaijan">Azerbaijan</option>
              <option value="Bahamas">Bahamas</option>
              <option value="Bahrain">Bahrain</option>
              <option value="Bangladesh">Bangladesh</option>
              <option value="Barbados">Barbados</option>
              <option value="Belarus">Belarus</option>
              <option value="Belgium">Belgium</option>
              <option value="Belize">Belize</option>
              <option value="Benin">Benin</option>
              <option value="Bermuda">Bermuda</option>
              <option value="Bhutan">Bhutan</option>
              <option value="Bolivia">Bolivia</option>
              <option value="Bonaire, Saint Eustatius and Saba ">Bonaire, Saint Eustatius and Saba </option>
              <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
              <option value="Botswana">Botswana</option>
              <option value="Bouvet Island">Bouvet Island</option>
              <option value="Brazil">Brazil</option>
              <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
              <option value="British Virgin Islands">British Virgin Islands</option>
              <option value="Brunei">Brunei</option>
              <option value="Bulgaria">Bulgaria</option>
              <option value="Burkina Faso">Burkina Faso</option>
              <option value="Burundi">Burundi</option>
              <option value="Cambodia">Cambodia</option>
              <option value="Cameroon">Cameroon</option>
              <option value="Canada">Canada</option>
              <option value="Cape Verde">Cape Verde</option>
              <option value="Cayman Islands">Cayman Islands</option>
              <option value="Central African Republic">Central African Republic</option>
              <option value="Chad">Chad</option>
              <option value="Chile">Chile</option>
              <option value="China">China</option>
              <option value="Christmas Island">Christmas Island</option>
              <option value="Cocos Islands">Cocos Islands</option>
              <option value="Colombia">Colombia</option>
              <option value="Comoros">Comoros</option>
              <option value="Cook Islands">Cook Islands</option>
              <option value="Costa Rica">Costa Rica</option>
              <option value="Croatia">Croatia</option>
              <option value="Cuba">Cuba</option>
              <option value="Curacao">Curacao</option>
              <option value="Cyprus">Cyprus</option>
              <option value="Czech Republic">Czech Republic</option>
              <option value="Democratic Republic of the Congo">Democratic Republic of the Congo</option>
              <option value="Denmark">Denmark</option>
              <option value="Djibouti">Djibouti</option>
              <option value="Dominica">Dominica</option>
              <option value="Dominican Republic">Dominican Republic</option>
              <option value="East Timor">East Timor</option>
              <option value="Ecuador">Ecuador</option>
              <option value="Egypt">Egypt</option>
              <option value="El Salvador">El Salvador</option>
              <option value="Equatorial Guinea">Equatorial Guinea</option>
              <option value="Eritrea">Eritrea</option>
              <option value="Estonia">Estonia</option>
              <option value="Ethiopia">Ethiopia</option>
              <option value="Falkland Islands">Falkland Islands</option>
              <option value="Faroe Islands">Faroe Islands</option>
              <option value="Fiji">Fiji</option>
              <option value="Finland">Finland</option>
              <option value="France">France</option>
              <option value="French Polynesia">French Polynesia</option>
              <option value="French Southern Territories">French Southern Territories</option>
              <option value="Gabon">Gabon</option>
              <option value="Gambia">Gambia</option>
              <option value="Georgia">Georgia</option>
              <option value="Germany">Germany</option>
              <option value="Ghana">Ghana</option>
              <option value="Gibraltar">Gibraltar</option>
              <option value="Greece">Greece</option>
              <option value="Greenland">Greenland</option>
              <option value="Grenada">Grenada</option>
              <option value="Guadeloupe">Guadeloupe</option>
              <option value="Guam">Guam</option>
              <option value="Guatemala">Guatemala</option>
              <option value="Guernsey">Guernsey</option>
              <option value="Guinea">Guinea</option>
              <option value="Guinea-Bissau">Guinea-Bissau</option>
              <option value="Guyana">Guyana</option>
              <option value="French Guiana">Guyane</option>
              <option value="Haiti">Haiti</option>
              <option value="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option>
              <option value="Honduras">Honduras</option>
              <option value="Hong Kong">Hong Kong</option>
              <option value="Hungary">Hungary</option>
              <option value="Iceland">Iceland</option>
              <option value="India">India</option>
              <option value="Indonesia">Indonesia</option>
              <option value="Iran">Iran</option>
              <option value="Iraq">Iraq</option>
              <option value="Ireland">Ireland</option>
              <option value="Isle of Man">Isle of Man</option>
              <option value="Israel">Israel</option>
              <option value="Italy">Italy</option>
              <option value="Ivory Coast">Ivory Coast</option>
              <option value="Jamaica">Jamaica</option>
              <option value="Japan">Japan</option>
              <option value="Jersey">Jersey</option>
              <option value="Jordan">Jordan</option>
              <option value="Kazakhstan">Kazakhstan</option>
              <option value="Kenya">Kenya</option>
              <option value="Kiribati">Kiribati</option>
              <option value="Kosovo">Kosovo</option>
              <option value="Kuwait">Kuwait</option>
              <option value="Kyrgyzstan">Kyrgyzstan</option>
              <option value="Laos">Laos</option>
              <option value="Latvia">Latvia</option>
              <option value="Lebanon">Lebanon</option>
              <option value="Lesotho">Lesotho</option>
              <option value="Liberia">Liberia</option>
              <option value="Libya">Libya</option>
              <option value="Liechtenstein">Liechtenstein</option>
              <option value="Lithuania">Lithuania</option>
              <option value="Luxembourg">Luxembourg</option>
              <option value="Macao">Macao</option>
              <option value="Macedonia">Macedonia</option>
              <option value="Madagascar">Madagascar</option>
              <option value="Malawi">Malawi</option>
              <option value="Malaysia">Malaysia</option>
              <option value="Maldives">Maldives</option>
              <option value="Mali">Mali</option>
              <option value="Malta">Malta</option>
              <option value="Marshall Islands">Marshall Islands</option>
              <option value="Martinique">Martinique</option>
              <option value="Mauritania">Mauritania</option>
              <option value="Mauritius">Mauritius</option>
              <option value="Mayotte">Mayotte</option>
              <option value="Mexico">Mexico</option>
              <option value="Micronesia">Micronesia</option>
              <option value="Moldova">Moldova</option>
              <option value="Monaco">Monaco</option>
              <option value="Mongolia">Mongolia</option>
              <option value="Montenegro">Montenegro</option>
              <option value="Montserrat">Montserrat</option>
              <option value="Morocco">Morocco</option>
              <option value="Mozambique">Mozambique</option>
              <option value="Myanmar">Myanmar</option>
              <option value="Namibia">Namibia</option>
              <option value="Nauru">Nauru</option>
              <option value="Nepal">Nepal</option>
              <option value="Netherlands">Netherlands</option>
              <option value="Netherlands Antilles">Netherlands Antilles</option>
              <option value="Serbia and Montenegro">Netherlands Antilles</option>
              <option value="New Caledonia">New Caledonia</option>
              <option value="New Zealand">New Zealand</option>
              <option value="Nicaragua">Nicaragua</option>
              <option value="Niger">Niger</option>
              <option value="Nigeria">Nigeria</option>
              <option value="Niue">Niue</option>
              <option value="Norfolk Island">Norfolk Island</option>
              <option value="North Korea">North Korea</option>
              <option value="Northern Mariana Islands">Northern Mariana Islands</option>
              <option value="Norway">Norway</option>
              <option value="Oman">Oman</option>
              <option value="Pakistan">Pakistan</option>
              <option value="Palau">Palau</option>
              <option value="Palestinian Territory">Palestine</option>
              <option value="Panama">Panama</option>
              <option value="Papua New Guinea">Papua New Guinea</option>
              <option value="Paraguay">Paraguay</option>
              <option value="Peru">Peru</option>
              <option value="Philippines">Philippines</option>
              <option value="Pitcairn">Pitcairn</option>
              <option value="Poland">Poland</option>
              <option value="Portugal">Portugal</option>
              <option value="Puerto Rico">Puerto Rico</option>
              <option value="Qatar">Qatar</option>
              <option value="Republic of the Congo">Republic of the Congo</option>
              <option value="Reunion">Reunion</option>
              <option value="Romania">Romania</option>
              <option value="Russia">Russia</option>
              <option value="Rwanda">Rwanda</option>
              <option value="Saint Barth&#233;lemy">Saint Barth&#233;lemy</option>
              <option value="Saint Helena">Saint Helena</option>
              <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
              <option value="Saint Lucia">Saint Lucia</option>
              <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
              <option value="Saint Martin">Saint-Martin</option>
              <option value="Saint Pierre and Miquelon">Saint-Pierre et Miquelon</option>
              <option value="Samoa">Samoa</option>
              <option value="San Marino">San Marino</option>
              <option value="Sao Tome and Principe">Sao Tome and Principe</option>
              <option value="Saudi Arabia">Saudi Arabia</option>
              <option value="Senegal">Senegal</option>
              <option value="Serbia">Serbia</option>
              <option value="Seychelles">Seychelles</option>
              <option value="Sierra Leone">Sierra Leone</option>
              <option value="Singapore">Singapore</option>
              <option value="Sint Maarten">Sint Maarten</option>
              <option value="Slovakia">Slovakia</option>
              <option value="Slovenia">Slovenia</option>
              <option value="Solomon Islands">Solomon Islands</option>
              <option value="Somalia">Somalia</option>
              <option value="South Africa">South Africa</option>
              <option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
              <option value="South Korea">South Korea</option>
              <option value="Spain">Spain</option>
              <option value="Sri Lanka">Sri Lanka</option>
              <option value="Sudan">Sudan</option>
              <option value="Suriname">Suriname</option>
              <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
              <option value="Swaziland">Swaziland</option>
              <option value="Sweden">Sweden</option>
              <option value="Switzerland">Switzerland</option>
              <option value="Syria">Syria</option>
              <option value="Taiwan">Taiwan</option>
              <option value="Tajikistan">Tajikistan</option>
              <option value="Tanzania">Tanzania</option>
              <option value="Thailand">Thailand</option>
              <option value="Togo">Togo</option>
              <option value="Tokelau">Tokelau</option>
              <option value="Tonga">Tonga</option>
              <option value="Trinidad and Tobago">Trinidad and Tobago</option>
              <option value="Tunisia">Tunisia</option>
              <option value="Turkey">Turkey</option>
              <option value="Turkmenistan">Turkmenistan</option>
              <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
              <option value="Tuvalu">Tuvalu</option>
              <option value="U.S. Virgin Islands">U.S. Virgin Islands</option>
              <option value="Uganda">Uganda</option>
              <option value="Ukraine">Ukraine</option>
              <option value="United Arab Emirates">United Arab Emirates</option>
              <option value="United Kingdom">United Kingdom</option>
              <option value="United States">United States</option>
              <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
              <option value="Uruguay">Uruguay</option>
              <option value="Uzbekistan">Uzbekistan</option>
              <option value="Vanuatu">Vanuatu</option>
              <option value="Vatican">Vatican</option>
              <option value="Venezuela">Venezuela</option>
              <option value="Vietnam">Vietnam</option>
              <option value="Wallis and Futuna">Wallis et Futuna</option>
              <option value="Western Sahara">Western Sahara</option>
              <option value="Yemen">Yemen</option>
              <option value="Zambia">Zambia</option>
              <option value="Zimbabwe">Zimbabwe</option>
            </select> <!-- #country -->
          </div> <!-- .form-item -->
          <div class="form-item">
            <div id="donate-amount">
              <label for="">Choose U.S. Dollar Amount</label>
            </div>
            <span class="donate-amount">
              <label>
                <input type="radio" id="id_amount_1" value="1" name="amount" class="amount-select">
                $1
              </label>
            </span> <!-- .donate-amount -->
            <span class="donate-amount">
              <label>
                <input type="radio" id="id_amount_5" value="5" name="amount" class="amount-select">
                $5
              </label>
            </span> <!-- .donate-amount -->
            <span class="donate-amount">
              <label>
                <input type="radio" id="id_amount_10" value="10" name="amount" class="amount-select">
                $10
              </label>
            </span> <!-- .donate-amount -->
            <span class="donate-amount">
              <label>
                <input type="radio" id="id_amount_25" value="25" name="amount" class="amount-select">
                $25
              </label>
            </span> <!-- .donate-amount -->
            <span class="donate-amount">
              <label>
                <input type="radio" id="id_amount_50" value="50" name="amount" class="amount-select">
                $50
              </label>
            </span> <!-- .donate-amount -->
            <span class="donate-amount">
              <label>
                <input type="radio" id="id_amount_100" value="100" name="amount" class="amount-select">
                $100
              </label>
            </span> <!-- .donate-amount -->
            <span class="donate-amount">
              <label>
                <input type="radio" id="id_amount_150" value="150" name="amount" class="amount-select">
                $150
              </label>
            </span> <!-- .donate-amount -->
            <span class="donate-amount">
              <label>
                <input type="radio" id="id_amount_250" value="250" name="amount" class="amount-select">
                $250
              </label>
            </span> <!-- .donate-amount -->
            <span class="donate-amount">
              <noscript><input type="radio" value="" class="amount_radio_button" name="amount"></noscript>
              <label>
                Other:
                <input type="text" class="form-text" id="amount_other_field" name="amount" size="3" class="amount-other amount-select">
              </label>
            </span> <!-- .donate-amount -->
          </div> <!-- .form-item -->
          <div class="form-item">
            <div>
              <label for="donation_type">Donation Type</label>
            </div>
            <span class="donate-type">
              <label>
                <input type="radio" name="donation_type" id="id_donation_type_single" value="single" checked> One-time
              </label>
            </span> <!-- .donate-type -->
            <span class="donate-type">
              <label>
                <input type="radio" name="donation_type" id="id_donation_type_recurring" value="recurring"> Monthly (recurring)
              </label>
            </span> <!-- .donate-type -->
          </div> <!-- .form-item -->
          <div class="form-item">
            <label for="card_num">Credit Card #</label>
            <input id="card_num" type="text" name="card_num" class="form-text required creditcard">
          </div> <!-- .form-item -->
          <div class="form-item">
            <label for="card_code">Verification Code</label>
            <input id="card_code" type="text" name="card_code" size="4" class="form-text required">
          </div> <!-- .form-item -->
          <div class="form-item">
            <div>
              <label for="exp_date_month">Expiration Date</label>
            </div>
            <select id="exp_date_month" type="text" name="exp_date_month" class="form-select required" style="width: 25%">
              <option value=""></option>
              <option value="01">01</option>
              <option value="02">02</option>
              <option value="03">03</option>
              <option value="04">04</option>
              <option value="05">05</option>
              <option value="06">06</option>
              <option value="07">07</option>
              <option value="08">08</option>
              <option value="09">09</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
            </select> <!-- #exp_date_month -->
            <select id="exp_date_year" type="text" name="exp_date_year" class="form-select required" style="width: 40%">
              <option value=""></option>
              <option value="13">2013</option>
              <option value="14">2014</option>
              <option value="15">2015</option>
              <option value="16">2016</option>
              <option value="17">2017</option>
              <option value="18">2018</option>
              <option value="19">2019</option>
              <option value="20">2020</option>
              <option value="21">2021</option>
              <option value="22">2022</option>
              <option value="23">2023</option>
              <option value="24">2024</option>
              <option value="25">2025</option>
            </select> <!-- #exp_date_year -->
          </div> <!-- .form-item -->
          <?php /*
          <div class="form-item">
            <p>
              <span class="label alert">
                Please only click the donation button once!
              </span>
            </p>
          </div> <!-- .form-item -->
          */ ?>
          <div class="form-item">
            <input id="submitbutton" type="submit" class="button large donate-form-submit" value="Submit Donation">
          </div> <!-- .form-item -->
        </form> <!-- #donate-form -->
      </div> <!-- .six.columns -->
    </div> <!-- #donate-page -->
  </div> <!-- .row -->
</div> <!-- #node-X -->

<?php /*
<div id="processing">
  <div class="process-text">Processing... please wait.</div>
</div> <!-- #processing --> */ ?>

<script type="text/javascript">
</script>
